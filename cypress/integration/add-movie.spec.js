const apiURL = "https://movielovers-staging.herokuapp.com/movie";

describe("Add Movie page", () => {
    beforeEach(() => {
        cy.request(apiURL)
            .its("body")
            .then(movies => {
                movies.forEach(element => {
                    cy.request("DELETE", `${apiURL}${element._id}`);
                });
            });
        cy.fixture("movies").then(movies => {
            let [d1, d2, d3, d4, ...rest] = movies;
            let four = [d1, d2, d3, d4];
            four.forEach(movie => {
                cy.request("POST", apiURL, movie);
            });
        });
        cy.visit("/");
        // Click add movie navbar link
        cy.get(".navbar-nav")
            .eq(0)
            .find(".nav-item")
            .eq(4)
            .click();
    });
    describe("Add a movie", () => {
        describe("With valid attributes", () => {
            it("allows movie to be added", () => {
                //  Fill out web form
                cy.get("input[data-test=title]").type("Work please");
                cy.get("input[data-test=released]").type("2019");
                cy.get("input[data-test=cost]").type(15);
                cy.get("input[data-test=stock]").type(10);
                cy.contains("Thanks for your Adding!").should("not.exist");
                cy.get(".error").should("not.exist");
                cy.get("button[type=submit]").click();
                cy.contains("Thanks for your Adding!").should("exist");
            });
            after(() => {
                cy.wait(100)
                // Click Manage movie
                cy.get(".navbar-nav")
                    .eq(0)
                    .find(".nav-item")
                    .eq(1)
                    .click();
                cy.get("tbody")
                    .find("tr")
                    .should("have.length", 5);
            });
        });
        describe("With invalid/blank attributes", () => {
            it("shows error messages until all attributes are ", () => {
                cy.get("button[type=submit]").click();
                cy.contains("Please Fill in the Form Correctly.").should("exist");
            });


            after(() => {
                cy.wait(100)
                // Click Manage Movies
                cy.get(".navbar-nav")
                    .eq(0)
                    .find(".nav-item")
                    .eq(1)
                    .click();
                cy.get("tbody")
                    .find("tr")
                    .should("have.length", 4);
            });
        });
    });
});

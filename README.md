# Assignment 2 - Agile Software Practice.

Name: Ryan Mckenna

## Client UI.



![][home]
>>Home page 

![][movies]
>>Allows the user manage movies purchase/delete

![][shows]
>>Allows the user manage shows purchase/delete
>
![][reviews]
>>Allows the user manage reviews purchase/delete
>
![][addm]
>>Allows the user add a new movie
>
![][adds]
>>Allows the user add a new show
>
![][addr]
>>Allows the user add a new review
>
![][edit]
>>Allows the user to edit review


## E2E/Cypress testing.
![][test]
Was unable to get tests running due to this error.

## Web API CI.
gitlab: https://gitlab.com/RyanMckenna95/movieapi-test <br>
development page: https://movielovers-staging.herokuapp.com/ <br>
production page: https://movielovers-pro.herokuapp.com/
## GitLab CI.




[home]: ./img/homepage.JPG
[movies]: ./img/movieman.JPG
[shows]: ./img/showman.JPG
[reviews]: ./img/reviewman.JPG
[addm]: ./img/addmovie.JPG
[adds]: ./img/addshow.JPG
[addr]: ./img/addreview.JPG
[edit]: ./img/edit.JPG
[test]: ./img/test.JPG
